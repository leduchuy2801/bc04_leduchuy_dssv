const DSSV_LOCALSTORAGE = "DSSV_LOCALSTORAGE";

var dssv = [];
var dssvJson = localStorage.getItem(DSSV_LOCALSTORAGE);
if (dssvJson != null) {
  dssv = JSON.parse(dssvJson);
  // *****array khi convert thanhf json sẽ mất funtion , t a map lại ***
  for (var index = 0; index < dssv.length; index++) {
    sv = dssv[index];
    dssv[index] = new SinhVien(
      sv.ten,
      sv.ma,
      sv.matkhau,
      sv.email,
      sv.toan,
      sv.hoa,
      sv.ly
    );
  }
  renderDSSV(dssv);
}
function themSV() {
  var newSv = layThongTinTuForm();

  // Kiểm tra rỗng : ***
  var isValid =
    validation.kiemTraRong(newSv.ma, "spanMaSV", "Ma sv k dc de rong") &&
    validation.kiemTraDoDai(
      newSv.ma,
      "spanMaSV",
      "ma sv phai co 4 ky tu ",
      (min = 4),
      (max = 4)
    );

  var isValid =
    isValid &
    validation.kiemTraRong(newSv.ten, "spanTenSV", "ten sv k dc de rong");
  var isValid =
    isValid &
    validation.kiemTraRong(
      newSv.email,
      "spanEmailSV",
      "email  sv k dc de rong"
    );
  //  end ***
  if (isValid) {
    dssv.push(newSv);
    //   console.log("newSv", newSv);
    var dssvJson = JSON.stringify(dssv);
    // lưu json vào localStorage

    localStorage.setItem(DSSV_LOCALSTORAGE, dssvJson);
    renderDSSV(dssv);
    console.log("dssv : ", dssv);
  }
}
function xoaSinhVien(id) {
  console.log(id);
  var index = timKiemViTri(id, dssv);
  console.log(index);
  dssv.splice(index, 1);
  renderDSSV(dssv);
}
function suaSinhVien(id) {
  var index = timKiemViTri(id, dssv);
  console.log("index", index);
  if (index != -1) {
    var sv = dssv[index];
    showThongTinLenForm(sv);
  }
  
}
